﻿$(document).ready(function () {
    /*var manualuploader = new qq.FineUploader({
        element: $('#manual-profile-fine-uploader')[0],
        request: {
            endpoint: 'server/handleUploads'
        },
        autoUpload: false,
        text: {
            uploadButton: '<i class="icon-plus icon-white"></i> Select Files'
        },
        debug: true
    });

    $('#btn_updateprofilepic').click(function () {
        manualuploader.uploadStoredFiles();
    });*/


    $('input#profilephotouploader[type=file]').change(function () {

        $(this).simpleUpload("/ajax/upload.ashx", {

            start: function (file) {
                //upload started
                console.log("upload started");
            },

            progress: function (progress) {
                //received progress
                console.log("upload progress: " + Math.round(progress) + "%");
            },

            success: function (data) {
                //upload successful
                console.log("upload successful!");
                console.log(data);
            },

            error: function (error) {
                //upload failed
                console.log("upload error: " + error.name + ": " + error.message);
            }

        });

    });


    $('input#coverphotouploader[type=file]').change(function () {

        $(this).simpleUpload("/ajax/upload.ashx", {

            start: function (file) {
                //upload started
                console.log("upload started");
            },

            progress: function (progress) {
                //received progress
                console.log("upload progress: " + Math.round(progress) + "%");
            },

            success: function (data) {
                //upload successful
                console.log("upload successful!");
                console.log(data);
            },

            error: function (error) {
                //upload failed
                console.log("upload error: " + error.name + ": " + error.message);
            }

        });

    });

});
/*
$(function() {

    $('form').submit(function() {

        // Move cropped image data to hidden input
        var imageData = $('.image-editor').cropit('export');
        $('.hidden-image-data').val(imageData);

        // Print HTTP request params
        var formValue = $(this).serialize();
        $('#result-data').text(formValue);

        // ^   the codes of Cropit extension
        // v   the codes of PHP/OC3 for uploading images

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file[]" value="" multiple="multiple" /></form>');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function() {
            if (imageData != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=common/filemanager/upload&user_token={{ user_token }}&directory={{ directory }}',
                    type: 'post',
                    dataType: 'json',
                    data: FormData(formValue), //new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                    },
                    complete: function() {
                    },
                    success: function(json) {
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                    }
                });
            }
        }, 500);

        // Prevent the form from actually submitting
        return false;
    });
});*/