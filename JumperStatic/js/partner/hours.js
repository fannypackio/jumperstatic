﻿$(document).ready(function () {
    $("#AddHours").click(function () {
        $("#extra-hidden-hours>.component-business-hours").clone().appendTo($("[id$=colhours]"));
    });
    $(".fa.fa-trash").click(function () {
        var clickedElement = $(event.target);
        clickedElement.parents(".row-flex").remove();
    });
    $("#SaveHours").click(function () {
        var isValidHrs = validateHours();
        //showLoaderDemo();
        if (isValidHrs) {
            //Call Api to save hrs
            //console.log(JSON.stringify(jsonList));
            /*$.post('//localhost:54397/api/Partners/UpdateHours', JSON.stringify(jsonList), function (response) {
                // Do something with the request
                $(".admin-component-hours").removeClass("edit-active");
                $(".enabler").attr("disabled", "true");
            }, 'json');
            */
            $("body").addClass("loading");
            $.ajax({
                type: "POST",
                url: "/ajax/partnerhours.ashx",
                // The key needs to match your method's input parameter (case-sensitive).
                data: JSON.stringify(jsonList),
                contentType: "application/json; charset=utf-8",
                //dataType: "json",
                success: function (data) {
                    //alert(data);
                    $("body").removeClass("loading");
                    showSuccessNotification("Hours Updated");
                    $("#CancelHours").click();
                },
                failure: function (errMsg) {
                    //alert(errMsg);
                    $("body").removeClass("loading");
                    showErrorNotification("Something went wrong, Try again!");
                }
            });

        }
    });

    // Edit Hours

    $("#EditHours").click(function () {
        $(this).closest(".admin-component-hours").addClass("edit-active");
        $(".enabler").removeAttr("disabled");
    });
    $("#CancelHours").click(function () {
        $(this).closest(".admin-component-hours").removeClass("edit-active");
        $(".enabler").attr("disabled", "true");
    });
});
var jsonList = [];

function validateHours() {
    var valid = true;
    jsonList = [];
    $("[id$=colhours] .row-flex").each(function (index, el) {
        $(el).toggleClass("error", false);
        var startTime = $(el).find(".starttime").val();
        var endTime = $(el).find(".endtime").val();

        var start = startTime.split(':');
        var startminutes = parseInt(start[0]) * 60 + parseInt(start[1]);

        var end = endTime.split(':');
        var endminutes = parseInt(end[0]) * 60 + parseInt(end[1]);

        if (endminutes <= startminutes) {
            valid = false;
            $(el).toggleClass("error", true);
            showErrorNotification("Invalid hours");
        } else {
            var day = $(el).find(".daySelect").val();
            jsonList.push(new ophours(day, startminutes, endminutes));
            $(el).css('background-color', "");
        }
    });
    return valid;
}
function ophours(day,starttime,endtime){
    this.Day = day;
    this.StartTime = starttime;
    this.EndTime = endtime;
    this.PartnerID = $("[id$=hdn_PartnerID]").val();
}
