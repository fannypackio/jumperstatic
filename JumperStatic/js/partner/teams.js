﻿$(document).ready(function () {

    $("#invitelist").tagsInput();

    // Adding team AddMembersButton

    $("#AddMembersButton").click(function () {
        $(".component-add-member").addClass("edit-active");
    });
    $(".cta-cancel-add").click(function () {
        $(".component-add-member").removeClass("edit-active");
    });
    $("#SendInvitesButton").click(function () {
        //showLoaderDemo();
        InviteTeam();
        $(".component-add-member").removeClass("edit-active");
    });
    $(".empavialability").click(function (event) {
        var el = event.target;
        var isAvailable = parseInt($(event.currentTarget).data("avialable"));
        var empid = $(event.currentTarget).data("empid")
        isAvailable = (isAvailable == 0) ? 1 : 0;
        if (isAvailable == 1) {
            $("#dialog-confirm-available").data("elevent", event).dialog({
                resizable: false,
                height: "auto",
                width: 500,
                draggable: false,
                modal: true,
                buttons: {
                    "Confirm": function () {
                        changeStatus(empid, isAvailable, event);
                        $(this).dialog("close");
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                }
            });
        } else {
            $("#dialog-confirm-unavailable").data("elevent", event).dialog({
                resizable: false,
                height: "auto",
                width: 500,
                draggable: false,
                modal: true,
                buttons: {
                    "Confirm": function () {
                        changeStatus(empid, isAvailable, event);
                        $(this).dialog("close");
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }

    });
    $(".empdelete").click(function (event) {
        var empid = $(event.currentTarget).data("empid")
        $("#dialog-confirm-delete").data("elevent",event).dialog({
            resizable: false,
            height: "auto",
            width: 500,
            draggable: false,
            modal: true,
            buttons: {
                "Confirm": function () {
                    deleteEmployee(empid, event);
                    $(this).dialog("close");
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });

    });
});
function InviteTeam() {
    var inviteList = $("#invitelist").val();
    var code = $("[id$=hdn_PartnerInviteCode]").val();
    var name = $("[id$=hdn_PartnerName]").val();
    $("body").addClass("loading");
    $("#invitelist").removeTag();
    $.ajax({
        type: "POST",
        url: "/ajax/inviteteam.ashx",
        //url: "https://partnerapi.hellojumper.com/api/account/InviteTeam",
        // The key needs to match your method's input parameter (case-sensitive).
        data: JSON.stringify(new EmailList(inviteList,code,name)),
        contentType: "application/json; charset=utf-8",
        //dataType: "json",
        success: function (data) {
            var arrList = inviteList.split(",");
            $.each(arrList, function (i,e) { $("#invitelist").removeTag(e); });

            $("body").removeClass("loading");
            showSuccessNotification("Invitation sent!");
        },
        failure: function (errMsg) {
            $("body").removeClass("loading");
            alert(errMsg);
        }
    });

}

function EmailList(list,code,name) {
    this.List = list;
    this.Code = code;
    this.Name = name;
}

function Employee(id, available) {
    this.Id = id;
    this.IsAvailable = available;
}

function changeStatus(empid, isAvailable,evnt) {
    $("body").addClass("loading");
    $.ajax({
        type: "GET",
        url: "/ajax/empstatus.ashx?id=" + empid + "&available=" + isAvailable,
        // The key needs to match your method's input parameter (case-sensitive).
        //data: JSON.stringify(new Employee($(event.currentTarget).data("empid"), isAvailable)),
        //contentType: "application/json; charset=utf-8",
        //dataType: "json",
        /*beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "bearer " + readCookie("PartnerCookie"));
        },*/
        success: function (data) {
           // alert("asdasd");
            $("body").removeClass("loading");
            if (isAvailable == 1) {
                $(evnt.currentTarget).data("avialable", isAvailable);
                $(evnt.currentTarget).text("Make Unavailable").toggleClass("cta-red", true).toggleClass("cta-green-text", false);
            } else {
                $(evnt.currentTarget).data("avialable", isAvailable);
                $(evnt.currentTarget).text("Make Available").toggleClass("cta-red", false).toggleClass("cta-green-text", true);
            }
            //Hide Activity Indicator
        },
        failure: function (errMsg) {
            //alert(errMsg);
            $("body").removeClass("loading");
            //Hide Activity Indicator
        }
    });
}

function deleteEmployee(empid,evnt) {
    $("body").addClass("loading");
    $.ajax({
        type: "GET",
        url: "/ajax/empremove.ashx?id=" + empid,
        // The key needs to match your method's input parameter (case-sensitive).
        //data: JSON.stringify(new Employee($(event.currentTarget).data("empid"))),
        //contentType: "application/json; charset=utf-8",
        //dataType: "json",
        /*beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "bearer DT_J5r5umM3N-88riHKRjeD1ImaOAM0YUSHJKdWRyWnc1Ok8Fl9plT_tmMO9sAYsgXuhV9E1nAeEieyLAmciBXtn7xm-1mIxlyhSv89Zo09iJIG1Xnj_8lQrC3vj6KtTmcTt7Ip8sXVb5W7iX0cua2cflZcUu-3hp784D-p88AVJps7TN0ivlepGpAz3dT-dOsfxMWtm7nSZa__9jJqJfjOFTKAMJwlek71X0hRfmgAm8ATyflzbGSz0HobJNr6XSVmCCqcOdfkkh3XrjGD05cQKAvULpauoOftaiSg2LjTqfqhgUgqKVlp_l5Q-ILFjOeUfsScBm4UhxA0aZmRAUwsds1rXjklB5zoSi_dt2PncDaT4a9u7wjye9l0iQ2sjkL3z3HnJ-fyukViYFIWE4Dxrz9WJsqEOmlsR6rKrVL0hLKSVi78-XV_oDAnI-ehrE4kbSJKQ6FQieQ2YFrxpyJo44hsDWB3-4T06iAl65NXfniGpwPEa3Jt98pDihE7s");
        },*/
        success: function (data) {
            //alert(data);
            $(evnt.currentTarget).hide();
            $("body").removeClass("loading");
            //Hide Activity Indicator
        },
        failure: function (errMsg) {
           // alert(errMsg);
            $("body").removeClass("loading");
            //Hide Activity Indicator
        }
    });
}
