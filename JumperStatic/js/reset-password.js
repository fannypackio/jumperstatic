﻿$(document).ready(function(){
    if (window.location.href.indexOf("/partners/reset-password") > -1) {
        if (window.location.href.indexOf("?reset=") > -1) {

            $("#resetPasswordButton").bind("click", function () {
                $(".error-wrapper").removeClass("active");
                $(".error-wrapper").removeClass("success");

                if ( ($("#password1").val() == $("#password2").val()) && $(".password-input").val() != "") {
                    //$("body").addClass("no-flow");
                    //$(".success-customer").show();

                    var userCode = getParameterByName('code');
                    var userId = getParameterByName('userid');
                    var pass1 = $("#password1").val();
                    var pass2 = $("#password2").val();

                    var jqxhr = $.get("//partnerapi.hellojumper.com/api/Account/ResetForgotPassword?userid=" + userId + "&code=" + userCode + "&newpassword=" + pass1 + "&newcpassword=" + pass2 + "", function () {
                        //alert("success");
                        $(".component-password-titles").find("h3").text("You're all set!");
                        $(".component-password-titles").find("h4").text("Log into Jumper Partner using your new password");
                        $(".component-passwords").hide();
                    })
                    .fail(function (data) {
                        if (data.responseText != "") {
                            $(".error-wrapper").find("h4").text(data.responseJSON.Message);
                            displayResetPageError();
                        }
                        else {
                            $(".error-wrapper").find("h4").text("Something is wrong, Please try again.");
                            displayResetPageError();
                        }
                    })
                    .always(function () {
                        // alert( "finished" );
                        $("#password1").val('');
                        $("#password2").val('');
                    });

                } else {
                    $(".error-wrapper").find("h4").text("Passwords didn't match.");
                    displayResetPageError();
                }
            });


        } else {
            window.location.replace("http://hellojumper.com");
        }
    } else {
        if (window.location.href.indexOf("?reset=") > -1) {

            $("#resetPasswordButton").bind("click", function(){

                $(".error-wrapper").removeClass("active");
                $(".error-wrapper").removeClass("success");

                setTimeout(function(){
                    if ( ($("#password1").val() == $("#password2").val()) && $(".password-input").val() != "") {

                        var userCode = getParameterByName('code');
                        var userId = getParameterByName('userid');
                        var pass1 = $("#password1").val();
                        var pass2 = $("#password2").val();

                        var jqxhr = $.get("//customerapi.hellojumper.com/api/Account/ResetForgotPassword?userid=" + userId + "&code=" + userCode + "&newpassword=" + pass1 + "&newcpassword=" + pass2 + "", function () {
                            //alert( "success" );
                            $(".component-password-titles").find("h3").text("You're all set!");
                            $(".component-password-titles").find("h4").text("Log into Jumper using your new password");
                            $(".component-passwords").hide();
                        })
                        .fail(function (data) {
                            if (data.responseText != "") {
                                $(".error-wrapper").find("h4").text(data.responseJSON.Message);
                                displayResetPageError();
                            }
                            else {
                                $(".error-wrapper").find("h4").text("Something is wrong, Please try again.");
                                displayResetPageError();
                            }
                        })
                        .always(function () {
                            //alert( "finished" );
                            $("#password1").val('');
                            $("#password2").val('');
                        });

                    } else {
                        $(".error-wrapper").find("h4").text("Passwords didn't match.");
                        displayResetPageError();
                    }
                },300);

            });
        } else {
            window.location.replace("http://hellojumper.com");
        }       
    }
});

function displayResetPageError() {
    $(".error-wrapper").addClass("active");
    setTimeout(function () { $(".error-wrapper").removeClass("active"); }, 2500);
}