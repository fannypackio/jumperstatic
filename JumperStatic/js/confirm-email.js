﻿$(document).ready(function() {
    if (window.location.href.indexOf("/partners/confirm-email") > -1) {
        if (window.location.href.indexOf("?confirm=") > -1) {


            var userCode = getParameterByName('confirm');
            var userId = getParameterByName('userid');



            var jqxhr = $.get(window.keys["api.jumper.partner"] + "/account/ConfirmEmail?code=" + userCode + "&userid=" + userId + "", function () {
                //alert( "success" );
            })
            .fail(function () {
                //alert( "error" );
            })
            .always(function () {
                //alert( "finished" );
            });

        } else {
            window.location.replace("http://hellojumper.com");
        }
    } else {
        if (window.location.href.indexOf("?confirm=") > -1) {


            var userCode = getParameterByName('confirm');
            var userId = getParameterByName('userid');



            var jqxhr = $.get(window.keys["api.jumper.customer"] + "/account/ConfirmEmail?code=" + userCode + "&userid=" + userId + "", function () {
                // alert( "success" );
            })
            .fail(function () {
                $(".error-wrapper").addClass("active");
                $(".component-confirmed-email").find("h3").text("😐");
                $(".component-confirmed-email").find("h4").text("");
            })
            .always(function () {
                // alert( "finished" );
            });

        } else {
            window.location.replace("http://hellojumper.com");
        }
    }

});