
function initialize() {
    var inputPartner = document.getElementById('searchTextFieldPartner');
    new google.maps.places.Autocomplete(inputPartner);
    var inputCustomer = document.getElementById('searchTextFieldCustomer');
    new google.maps.places.Autocomplete(inputCustomer);
}
$(document).ready(function(){
    google.maps.event.addDomListener(window, 'load', initialize);
});
