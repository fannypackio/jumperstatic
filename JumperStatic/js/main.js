$(document).ready(function(){

    // Hover fallback for Mobile

    $(document).click(function(event) {
        if($(event.target).parents().hasClass('dd-header')) {
            if($(event.target).closest(".dd-header").hasClass('active')) {
                $(event.target).closest(".dd-header").removeClass("active");
                $(".dd-header").not($(event.target).closest(".dd-header")).removeClass("active");
            }
            else {
                $(event.target).closest(".dd-header").addClass("active");
                $(".dd-header").not($(event.target).closest(".dd-header")).removeClass("active");
            }
        }
        else {
            $(".dd-header").removeClass("active");
        }
    });
    $(".dd-header").hover(function(){
        $(".dd-header").removeClass("active");
    });

    // Mobile check

    (function(a){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);

    if (jQuery.browser.mobile) {
        $("#DownloadMobile").show();
        $("#LinkMobile").show();
    } else {
        $("#DownloadDesktop").show();
    }

    // Validating text link

    $("#TextAppLinkTo").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $(window).scroll( function() {
        var top = $(this).scrollTop();
        if (top > 0) {
            $(".header").addClass("alt");
        }
        else {
            $(".header").removeClass("alt");
        }
    });
    $("#btn_addcoupon").click(function () {
        event.preventDefault();
        $(".component-new-cc").addClass("edit-active");
        $("#btn_addcoupon").hide();
    });
    $(".cta-cancel-method").click(function(){
        $(".component-new-cc").removeClass("edit-active");
        $("#btn_addcoupon").show();
    });
    $("#UpdateCardButton").click(function(){
        $(".component-new-cc").removeClass("edit-active");
        showLoaderDemo();
    });
    //Promo code[id$=
    $("[id$=ChangePromoButton]").click(function () {
        $("[id$=ChangePromoButton]").hide();
        $(".component-new-promo").addClass("edit-active");
    });
    $(".cta-cancel-promo").click(function(){
        $(".component-new-promo").removeClass("edit-active");
        $("[id$=ChangePromoButton]").show();
    });
    /*
    $("#UpdatePromoButton").click(function(){
        $(".component-new-promo").removeClass("edit-active");
        showLoaderDemo();
    });
    */
    /*
    $(".cta-cancel-cc").click(function(){
        $(".component-new-cc").removeClass("edit-active");
    });
    */


    // Showing the confirmation overlay

    $(".overlay-trigger").click(function(){

        // Showing the overlay

        $("body").addClass("overlay-active");

        // Determining what type of overlay to show

        var type = $(this).attr("overlay-type");

        switch(type) {

            // Showing the "plan" overlay

            case "plan":

                // Finding which plan was selected and loading related content

                var theID = $(this).closest(".component-plan").attr("id");
                var price = $(this).closest(".component-plan").find(".price").text();

                $(".overlay").find("h3").text("Confirm Plan Change");

                switch(theID) {
                    case "PlanIndividual":
                        $(".overlay").find("h4").text("You'll be billed nothing going forward on this plan - any existing team members will be removed.");
                        break;
                    case "PlanSmall":
                        $(".overlay").find("h4").text("You'll be billed " + price + " per user, going forward.");
                        break;
                    case "PlanGrowing":
                        $(".overlay").find("h4").text("You'll be billed " + price + " per user, going forward.");
                        break;
                    case "PlanLarge":
                        $(".overlay").find("h4").text("You'll be billed " + price + " per user, going forward.");
                        break;
                }
                break;

            case "member-remove":

                // Finding the team member

                var memberName = $(this).closest(".team-member").find(".member-name").text();

                $(".overlay").find("h3").text("Remove Team Member");
                $(".overlay").find("h4").text("Are you sure you want to remove " + memberName + " from your account?");
                break;

            case "member-availability":

                // Finding the team member

                var memberName = $(this).closest(".team-member").find(".member-name").text();

                $(".overlay").find("h3").text("Turn off calls for this team member?");
                $(".overlay").find("h4").text("Are you sure you want to make " + memberName + " unavailable to receive calls?");
                break;


        }

    });

    // Hiding confirmation overlay

    $(".cta-overlay-close").click(function(){
        $("body").removeClass("overlay-active");
    });
    $(".cta-overlay-confirm").click(function(){
        showLoaderDemo();
    });

    // Switching subviews inside the pages

    $(".admin-component-link").click(function(){

        var theID = $(this).attr("id");

        $(".admin-component-link").removeClass("cta-underline");
        $(".admin-component").removeClass("active");

        switch(theID) {
            case "ComponentTeamLink":
                $(this).addClass("cta-underline");
                $(".admin-component-team-members").addClass("active");
                break;
            case "ComponentHoursLink":
                $(this).addClass("cta-underline");
                $(".admin-component-hours").addClass("active");
                loadhours();
                break;
            case "ComponentPlansLink":
                $(this).addClass("cta-underline");
                $(".admin-component-plans").addClass("active");
                break;
            case "ComponentBillingLink":
                $(this).addClass("cta-underline");
                $(".admin-component-billing").addClass("active");
                break;
            case "ComponentPhotosLink":
                $(this).addClass("cta-underline");
                $(".admin-component-photos").addClass("active");
            default:
                break;
        }
    });

    // Pill nav

    $(".pill-nav").click(function(){
        $(".header").toggleClass("header-open");
    });



    // Closing pill nav on mobile

    // $(".header .cta").click(function(){
    //     $(".header").removeClass("header-open");
    // });

    // Trimming inputs

    $("input:not([type='submit'])").on("blur", function(){
        var trim = $(this).val().trim();
        $(this).val(trim);
    });

    // TODO refactor this

    if($("#smsCheck").hasClass("sms-success")) {
        $(".error-wrapper h4").text("Check your phone for the link!");
        $(".error-wrapper").removeClass("success active");
        $(".error-wrapper").addClass("success active");
    }
    if($("#smsCheck").hasClass("sms-fail")) {
        $(".error-wrapper h4").text("Something went wrong. Try again!");
        $(".error-wrapper").removeClass("success active");
        $(".error-wrapper").addClass("active");
    }
    if(window.location.href.indexOf("success-partner") > -1) {
        $("body").addClass("no-flow");
        // $("body").addClass("partner");
        $(".success-partner").show();
    }
    $(".overlay-close").click(function(){
        $("body").removeClass("no-flow");
        $(".success-overlay").hide();
    });
    $(".overlay-close-partner").click(function(){
        window.location = window.location.href.replace('#success-partner', '');
    });
    $(".overlay-close-customer").click(function(){
        window.location = window.location.href.replace('#success-customer', '');
    });
    $(".overlay-close-customer").click(function(){
        window.location = window.location.href.replace('#success-customer', '');
    });
    $(".overlay-close-reset").click(function(){
        window.location.replace("/");
    });
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function showLoaderDemo() {

    // Add loading animation

    $("body").addClass("loading");

    // When loading finishes, add this to callback

    setTimeout(function () {
        $("body").addClass("loading-done");
    }, 2000);

    // Remove all loading classes

    setTimeout(function () {
        $("body").removeClass("loading");
        $("body").removeClass("loading-done");

        // Hiding other overlays

        $("body").removeClass("overlay-active");

    }, 3000);

}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function showErrorNotification(message) {
    $("#errorWrap.error-wrapper h4").text(message);
    $("#errorWrap.error-wrapper").toggleClass("active", true).toggleClass("success", false);

    setTimeout(function () {
        $("#errorWrap.error-wrapper").toggleClass("active", false).toggleClass("success", false);
    }, 3000);

}
function showSuccessNotification(message) {
    $("#errorWrap.error-wrapper h4").text(message);
    $("#errorWrap.error-wrapper").toggleClass("active", true).toggleClass("success", true);

    setTimeout(function () {
        $("#errorWrap.error-wrapper").toggleClass("active", false).toggleClass("success", false);
    }, 3000);
}
